package com.selenium.Base;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

public class BaseCucumberTest extends BaseClass{
		
	@BeforeTest
	public void StartTest() {
		initializeTest();
	}
	
	@AfterTest
	public void EndTest() {
		endTest();
	}
	
	/**
	 * 
	 */
}
