package com.selenium.Base;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.selenium.Utilities.BrowserFactory;
import com.selenium.Utilities.ConstantUtil;
import com.selenium.Utilities.FileManager;

public class BaseClass{
	
	public static WebDriver getDriver() {
		return BrowserFactory.getDriver();
	}
	
	/**
	 * Setup everything before every test class
	 */
	public void initializeTest() {
		FileManager.createFolder(ConstantUtil.TEMP_PATH);
		BrowserFactory.setBrowser(ConstantUtil.BROWSER_CHROME);
		BrowserFactory.openBrowser(ConstantUtil.COCHLEAR_URL);
	}
	
	/**
	 * End each test properly
	 */
	public void endTest() {
		BrowserFactory.closeBrowser();
	}
	
	public void performEnterText(By elementLocator, String value) {
		WebElement e = performWaitElementVisible(elementLocator);
		e.sendKeys(value);
		
	}
	
	public void performClick(By elementLocator) {
		WebElement e = performWaitElementVisible(elementLocator);
		e.click();
	}
	
	public void performSelectFromDropdown(By elementLocator, String value) {
		WebElement e = performWaitElementVisible(elementLocator);
		
		Select dropdown = new Select(e);
		dropdown.selectByVisibleText(value);
	}
	
	public void performDragAndDrop(By sourceLocator, By destinationLocator) {
		WebElement src = performWaitElementVisible(sourceLocator);
		WebElement dst = performWaitElementVisible(destinationLocator); 
		
		Actions action = new Actions(getDriver());
		action.dragAndDrop(src, dst).build().perform();		
	}
	
	public WebElement performWaitElementVisible(By locator) {
		WebDriverWait wait = new WebDriverWait(getDriver(), 30);
		return wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	
	public Boolean isElementExist(By locator) {
		Boolean flag = false;
				
//		try {
			WebDriverWait wait = new WebDriverWait(getDriver(), 30);
			if(wait.until(ExpectedConditions.visibilityOfElementLocated(locator)) != null) {
				flag = true;
			}
//		}catch(Exception e) {}
		
//		System.out.println(flag);
		
		return flag;
	}
			
	public void getScreenshot(String path) throws IOException {
		TakesScreenshot ss = (TakesScreenshot) getDriver();
		File src = ss.getScreenshotAs(OutputType.FILE);
		File dst = new File(path);
		FileUtils.copyFile(src, dst);
	}
	
	public void hardWait(Integer timeInSeconds) throws InterruptedException {
		Thread.sleep(timeInSeconds*1000);
	}
	
	/****
	 * Test
	 */
}
