package com.selenium.Base;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTest extends BaseClass{
		
	@BeforeClass
	public void StartTest() {
		initializeTest();
	}
	
	@AfterClass
	public void EndTest() {
		endTest();
	}
}
