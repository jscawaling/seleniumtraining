package com.selenium.Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BrowserFactory {
	
	private static WebDriver driver;
	
	public static void setBrowser(String browser) {
		if(browser.trim().equalsIgnoreCase("chrome")){
			setChromeDriver();				
		}
	}
	
	public static void openBrowser(String url) {	
		driver.get(url);		
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
	
	public static void setDriver(WebDriver d) {
		driver = d;
	}
	
	public static void closeBrowser() {
		driver.quit();
	}
	
	public static void setChromeDriver() {
		System.setProperty("webdriver.chrome.driver", ConstantUtil.CHROME_BROWSER_PATH);
		
		ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("start-maximized"); // open Browser in maximized mode
        chromeOptions.addArguments("disable-infobars"); // disabling infobars
        chromeOptions.addArguments("--disable-extensions"); // disabling extensions
        chromeOptions.addArguments("--disable-gpu"); // applicable to windows os only
        chromeOptions.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
        chromeOptions.addArguments("--no-sandbox"); // Bypass OS security model
        chromeOptions.addArguments("--disable-browser-side-navigation"); //https://stackoverflow.com/a/49123152/1689770
        chromeOptions.addArguments("--disable-geolocation");		
		
		driver = new ChromeDriver(chromeOptions);		
	}
}
