package com.selenium.Utilities;

import java.io.File;

public class ConstantUtil {

	public static final String CHROME_BROWSER_PATH = "resources" + System.getProperty("file.separator") + "driver" + System.getProperty("file.separator") + "chromedriver.exe";
	public static final String TEMP_PATH = "resources" + File.separator + "temp";
	
	public static final String BROWSER_CHROME = "chrome";
	
	public static final String COCHLEAR_URL = "https://reviewer:maryhasanangrylamb@dig-demo.stg.cochlear.cloud";
	
	
}
