package com.selenium.Utilities;

import java.io.File;

public class FileManager {
	
	/**
	 * Create a folder by first deleting it if exist
	 * @param path
	 * @return
	 */
	public static File createFolder(String path) {
		File f = new File(path);
		
		if(f.isDirectory()) {
			for(File item: f.listFiles()) {
				item.delete();
			}
			
			f.delete();
		}
				
		f.mkdir();
		
		return f;		
	}
	
	
}
