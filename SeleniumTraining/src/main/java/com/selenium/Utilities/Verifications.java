package com.selenium.Utilities;

import java.io.IOException;

import org.testng.Assert;

import com.selenium.Base.BaseClass;

public class Verifications extends BaseClass{
	
	public void verifyTrue(Boolean condition, String message) throws IOException {
		try {
			Assert.assertEquals(condition, Boolean.TRUE, message);
		}catch(AssertionError e) {
			Assert.fail(message, e);
		}
	}
	
	public void verifyFalse(Boolean condition, String message) throws IOException {
		try {
			Assert.assertEquals(condition, Boolean.FALSE, message);
		}catch(AssertionError e) {
			Assert.fail(message, e);
		}
	}
}
