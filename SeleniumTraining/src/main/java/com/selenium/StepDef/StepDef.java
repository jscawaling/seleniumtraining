package com.selenium.StepDef;

import java.io.IOException;
import java.util.Map;

import com.selenium.Base.BaseClass;
import com.selenium.Pages.ContactUsPage;
import com.selenium.Pages.GeneralInquiryPage;
import com.selenium.Pages.HomePage;
import com.selenium.Utilities.Verifications;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class StepDef{
	private final BaseClass control = new BaseClass();
	private final HomePage homePage = new HomePage();
	private final ContactUsPage contactUsPage = new ContactUsPage();
	private final GeneralInquiryPage generalInquiryPage = new GeneralInquiryPage();
	private final Verifications v = new Verifications();
		
	@Given("^I have test$")
	public void sampleTest() throws IOException {
		homePage.clickContactUs();
		contactUsPage.selectContactOption("General inquiry");
		contactUsPage.clickMakeAnOnlineInquiry();
		generalInquiryPage.setDescription("test");
		generalInquiryPage.setFirstName("Test");
		generalInquiryPage.setLastName("Auto");
		generalInquiryPage.setEmail("tesauto@email.com");
		generalInquiryPage.clickSend();
				
		v.verifyTrue(generalInquiryPage.isInqiurySubmitted(), "Verify if inquiry is successfully submitted");				
	}
	
	@Given("^I have logged into \"([^\"]*)\" environment using browser \"([^\"]*)\"$")
	public void i_have_logged_into(String environment, String browser) {
		
	}
		
	@And("^I Navigate to \"([^\"]*)\" page$")
	public void navigate_to(String pageName) {
		if(pageName.equalsIgnoreCase("Contact Us")) {
			homePage.clickContactUs();
		}
	}
	
	@And("^Select \"([^\"]*)\" from \"([^\"]*)\" dropdown in Contact Us page$")
	public void select_from_dropdown_contact_us(String itemToSelect, String dropdownName) {
		if(dropdownName.equalsIgnoreCase("contact option")) {
			contactUsPage.selectContactOption(itemToSelect);
		}
	}
	
	@And("^Click \"([^\"]*)\" button in Contact Us page$")
	public void click_button_in_contact_us(String buttonName) {
		
	}
	
	@And("^Fill \"([^\"]*)\" form with below date$")
	public void fill_form_with_below_data(String formName, DataTable dataRow) {
		
	}
	
	@Given("^I fill with below data$")
	public void fill_with_below_data(DataTable dataRow) {
		Map<String, String> map = dataRow.asMap(String.class, String.class);
		System.out.println(map.get("FIRST_NAME"));
		System.out.println(map.get("LAST_NAME"));
		
		
	}
	
	@Then("^Verify that inquiry is successfully submitted")
	public void verify_inquiry_submitted() throws IOException {
		v.verifyTrue(generalInquiryPage.isInqiurySubmitted(), "Verify if inquiry is successfully submitted");	
	}
}
