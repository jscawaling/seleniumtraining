package com.selenium.Pages;

import org.openqa.selenium.By;

import com.selenium.Base.BaseClass;

public class GeneralInquiryPage {
	private final BaseClass control = new BaseClass();
	
	private final By editDescription = By.id("commentc");
	private final By editFirstName = By.id("firstname");
	private final By editLastName = By.id("lastname");
	private final By editEmail = By.id("email");
	private final By buttonSend = By.id("submitBtn");
	
	private final By textThankYou = By.xpath("//div[contains(@class, 'form-box-success')]");
		
	public void setDescription(String value) {
		control.performEnterText(editDescription, value);
	}
	
	public void setFirstName(String value) {
		control.performEnterText(editFirstName, value);
	}
	
	public void setLastName(String value) {
		control.performEnterText(editLastName, value);
	}
	
	public void setEmail(String value) {
		control.performEnterText(editEmail, value);
	}
	
	public void clickSend() {
		control.performClick(buttonSend);
	}
	
	public Boolean isInqiurySubmitted() {
		return control.isElementExist(textThankYou);
	}
	
	public Boolean isSendDisplayed() {
		return control.isElementExist(buttonSend);
	}
}
