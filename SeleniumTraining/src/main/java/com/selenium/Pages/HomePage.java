package com.selenium.Pages;

import org.openqa.selenium.By;

import com.selenium.Base.BaseClass;

public class HomePage{
	
	private final BaseClass control = new BaseClass();
	
	private final By linkContactUs = By.xpath("//a[text()='Contact us']");
	private final By linkFindAClinic = By.xpath("//a[text()='Find a clinic']");
	private final By linkAboutUS = By.xpath("//a[text()='About us']");
	private final By linkStore = By.xpath("//a[text()='Store']");
	
	public void clickContactUs() {
		control.performClick(linkContactUs);
	}
	
	public void clickFindAClinic() {
		control.performClick(linkFindAClinic);
	}
	
	public void clickAboutUs() {
		control.performClick(linkAboutUS);
	}
	
}
