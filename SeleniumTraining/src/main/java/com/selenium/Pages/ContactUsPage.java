package com.selenium.Pages;

import org.openqa.selenium.By;

import com.selenium.Base.BaseClass;

public class ContactUsPage {
	
	private final BaseClass control = new BaseClass();
	
	private final By listContactOption = By.id("txt-form");
	private final By buttonMakeAnOnlineInquiry = By.xpath("//span[text()='Make an online inquiry']/parent::a");
	
	public void selectContactOption(String value) {
		control.performSelectFromDropdown(listContactOption, value);
	}
	
	public void clickMakeAnOnlineInquiry() {
		control.performClick(buttonMakeAnOnlineInquiry);
	}
}
