package com.selenium.Test;

import java.io.IOException;

import org.testng.annotations.Test;

import com.selenium.Base.BaseTest;
import com.selenium.Pages.ContactUsPage;
import com.selenium.Pages.GeneralInquiryPage;
import com.selenium.Pages.HomePage;
import com.selenium.Utilities.Verifications;

public class SampleTest extends BaseTest{
	
	private final HomePage homePage = new HomePage();
	private final ContactUsPage contactUsPage = new ContactUsPage();
	private final GeneralInquiryPage generalInquiryPage = new GeneralInquiryPage();
	private final Verifications v = new Verifications();
	
	@Test
	public void CreateGeneralInquiry() throws Exception {
		homePage.clickContactUs();
		contactUsPage.selectContactOption("General inquiry");
		contactUsPage.clickMakeAnOnlineInquiry();
		generalInquiryPage.setDescription("test");
		generalInquiryPage.setFirstName("Test");
		generalInquiryPage.setLastName("Auto");
		generalInquiryPage.setEmail("tesauto@email.com");
		generalInquiryPage.clickSend();
				
		v.verifyTrue(generalInquiryPage.isInqiurySubmitted(), "Verify if inquiry is successfully submitted");		
	}	
}
